/**
 * dom storage adapter
 * ===
 * - originally authored by Joseph Pecoraro
 *
 * - AMD module + Compression added by Steven Rogers
 *
 */

define(function (require) {
    'use strict';

    var Lawnchair = require('Lawnchair');
    var LZString = require('LZString');

    Lawnchair.adapter('dom-compress', (function() {
        // in case of cross-domain security violation
        // you can't even refence localStorage...
        var storage;
        try {
            storage = window.localStorage;
        } catch (e) {
            console.warn('Using cross-domain shell, values are not persisted!');
            storage = {
                setItem: function () { },
                getItem: function () { },
                removeItem: function () { }
            };
        }

        // the indexer is an encapsulation of the helpers needed to keep an ordered index of the keys
        var indexer = function(name) {
            return {
                // the key
                key: name + '._index_',

                // returns the index
                all: function() {
                    var a  = storage.getItem(JSON.stringify(this.key));

                    if (a) {
                        a = JSON.parse(a);
                    }

                    if (a === null) {
                        // lazy init
                        storage.setItem(JSON.stringify(this.key), JSON.stringify([]));
                    }

                    return JSON.parse(storage.getItem(JSON.stringify(this.key)));
                },

                // adds a key to the index
                add: function (key) {
                    var a = this.all();
                    a.push(key);

                    storage.setItem(JSON.stringify(this.key), JSON.stringify(a));
                },

                // deletes a key from the index
                del: function (key) {
                    var a = this.all(), r = [];

                    // FIXME this is crazy inefficient but I'm in a strata meeting and half concentrating
                    for (var i = 0, l = a.length; i < l; i++) {
                        if (a[i] !== key) { r.push(a[i]); }
                    }

                    storage.setItem(JSON.stringify(this.key), JSON.stringify(r));
                },

                // returns index for a key
                find: function (key) {
                    var a = this.all();
                    for (var i = 0, l = a.length; i < l; i++) {
                        if (key === a[i]) { return i; }
                    }

                    return false;
                }
            };
        };

        // adapter api
        function process(value, key) {
            value = LZString.decompressFromUTF16(value);

            if (value) {
                var parsed;
                try {
                    parsed = JSON.parse(value);
                    value = parsed;
                } catch (e) {
                    console.error('Failed to parse:', value);
                }

                value.key = key;
            }

            return value;
        }

        return {

            // ensure we are in an env with localStorage
            valid: function () {
                return !!storage && (function() {
                  // in mobile safari if safe browsing is enabled, window.storage
                  // is defined but setItem calls throw exceptions.

                  var success = true;
                  var value = Math.random();

                  try {
                    storage.setItem(value, value);
                  } catch (e) {
                    success = false;
                  }

                  storage.removeItem(value);

                  //
                  return success;
                })();
            },

            init: function (options, callback) {
                this.indexer = indexer(this.name);
                if (callback) {
                    this.fn(this.name, callback).call(this, this);
                }
            },

            save: function (obj, callback) {
                var uuid = obj.key  || this.uuid();
                var key = this.name + '.' + uuid;

                // now we kill the key and use it in the store colleciton
                delete obj.key;

                // encode & compress object
                var store;
                try {
                    store = JSON.stringify(obj);
                } catch (e) {
                    console.error('Failed to stringify:', obj);
                }

                store = LZString.compressToUTF16(store);
                storage.setItem(key, store);

                // if the key is not in the index push it on
                if (this.indexer.find(key) === false) {
                    this.indexer.add(key);
                }

                // add the key back to the object
                obj.key = key.slice(this.name.length + 1);

                // pass obj to optional callback
                if (callback) {
                    this.lambda(callback).call(this, obj);
                }

                //
                return this;
            },

            batch: function (ary, callback) {
                var saved = [];

                // not particularily efficient but this is more for sqlite situations
                var func = function (r) { saved.push(r); };
                for (var i = 0, l = ary.length; i < l; i++) {
                    this.save(ary[i], func);
                }

                if (callback) {
                    this.lambda(callback).call(this, saved);
                }

                return this;
            },

            // accepts [options], callback
            // TODO options for limit/offset, return promise
            keys: function (callback) {
                if (!callback) { return this; }

                var name = this.name;
                var indices = this.indexer.all();
                var keys = [];

                // checking for the support of map
                if (Array.prototype.map) {
                    keys = indices.map(function (r) { return r.replace(name + '.', ''); });
                } else {
                    /*jshint forin:false*/
                    for (var key in indices) {
                        keys.push(key.replace(name + '.', ''));
                    }
                }

                this.fn('keys', callback).call(this, keys);

                return this;
            },

            get: function (key, callback) {
                var results = [];

                var keyWasArray = true;
                if (!this.isArray(key)) {
                    keyWasArray = false;
                    key = [key];
                }

                var k, obj, i;
                var ic = key.length;
                for (i = 0; i < ic; i++) {
                    k = this.name + '.' + key;

                    obj = storage.getItem(k);
                    obj = process(obj, k);

                    results.push(obj);
                }

                if (!keyWasArray) {
                    results = results.shift();
                }

                if (callback) {
                    this.lambda(callback).call(this, results);
                }

                return this;
            },

            exists: function (key, cb) {
                var wasFalse = (this.indexer.find(this.name + '.' + key) === false);
                var exists = wasFalse ? false : true;
                this.lambda(cb).call(this, exists);
                return this;
            },

            // NOTE adapters cannot set this.__results but plugins do
            // this probably should be reviewed
            all: function (callback) {
                var keys = this.indexer.all();
                var results = [];

                var o, k, i;
                var ic = keys.length;

                for (i =0; i < ic; i++) {
                    k = keys[i]; //v

                    o = storage.getItem(k);
                    o = process(o);

                    o.key = k.replace(this.name + '.', '');

                    results.push(o);
                }

                if (callback) {
                    this.fn(this.name, callback).call(this, results);
                }

                return this;
            },

            remove: function (keyOrArray, callback) {
                var self = this;
                if (this.isArray(keyOrArray)) {
                    // batch remove
                    var i, done = keyOrArray.length;
                    var removeOne = function(i) {
                        self.remove(keyOrArray[i], function() {
                            if ((--done) > 0) { return; }
                            if (callback) {
                                self.lambda(callback).call(self);
                            }
                        });
                    };
                    for (i=0; i < keyOrArray.length; i++) {
                        removeOne(i);
                    }
                    return this;
                }

                var uuid = ((keyOrArray.key) ? keyOrArray.key : keyOrArray);
                var key = this.name + '.' + uuid;

                this.indexer.del(key);
                storage.removeItem(key);

                if (callback) {
                    this.lambda(callback).call(this);
                }

                return this;
            },

            nuke: function (callback) {
                this.all(function(r) {
                    for (var i = 0, l = r.length; i < l; i++) {
                        this.remove(r[i]);
                    }
                    if (callback) {
                        this.lambda(callback).call(this);
                    }
                });
                return this;
            }
        };
    })());
});
