A modification of the standard [Lawnchair.js](http://brian.io/lawnchair/) DOM adaptor that adds [LZString](http://pieroxy.net/blog/pages/lz-string/index.html) compression. Impressive reduction gains for sizable strings.

This adaptor is also wrapped in an anonymous AMD module.